from enum import IntEnum
from flask import Flask, request
from celery.result import AsyncResult
from tasks import find_sequence

app = Flask(__name__, static_folder='client/dist', static_url_path='')

# API
class Status(IntEnum):
    LOADING = 1
    NOT_FOUND = 2
    READY = 3

# curl http://127.0.0.1:7123/api/search -d '{"sequence": "ATCG"}' -H 'Content-Type: application/json'
@app.route('/api/search', methods=['POST'])
def start_search():
    content = request.get_json(silent=True)
    sequence = content.get('sequence')
    task = find_sequence.delay(sequence)
    return { "id" : task.task_id }

# curl http://127.0.0.1:7123/api/search/<id> -H 'Content-Type: application/json'
@app.route('/api/search/<id>', methods=['GET'])
def get_search_result(id):
    res = AsyncResult(id)
    
    if not res.ready():
        return { "status": Status.LOADING }

    match = res.get()

    return { "status": Status.READY, "match": match }

# Static
@app.route("/")
def index():
    return app.send_static_file('index.html')