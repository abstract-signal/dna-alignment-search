export const STATUS = {
  LOADING: 1,
  NOT_FOUND: 2,
  READY: 3,
};

export async function search({ sequence }) {
  const response = await fetch("/api/search", {
    ...common,
    method: "POST",
    body: JSON.stringify({ sequence }),
  });

  const result = await response.json();
  const searchId = result.id;
  return searchId;
}

export async function searchResult({ searchId }) {
  const response = await fetch(`/api/search/${searchId}`, {
    ...common,
    method: "GET",
  });

  const result = await response.json();
  return result;
}

const common = {
  headers: {
    "Content-Type": "application/json",
  },
};
