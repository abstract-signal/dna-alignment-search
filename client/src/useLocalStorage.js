import { useEffect, useState } from "react";

// same as setState, but persists value in window.localStorage
export function useLocalStorage(defaultValue, key) {
  const storage = window.localStorage;

  const [value, setValue] = useState(() => {
    const value = storage.getItem(key);
    return value !== null ? JSON.parse(value) : defaultValue;
  });

  useEffect(() => {
    if (typeof value !== undefined) {
      storage.setItem(key, JSON.stringify(value));
    }
  }, [key, value]);

  return [value, setValue];
}
