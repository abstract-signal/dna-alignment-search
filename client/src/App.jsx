import { useEffect } from "react";
import { useCallback } from "react";
import { useState } from "react";

import * as api from "./api";
import { useLocalStorage } from "./useLocalStorage";

function Request({ id, request, onUpdate }) {
  async function fetch() {
    if (request.status !== api.STATUS.LOADING) {
      return;
    }
    const searchId = await api.search({ sequence: id });
    if (!searchId) {
      return;
    }
    async function recurse() {
      const result = await api.searchResult({ searchId });
      if (result.status === api.STATUS.LOADING) {
        setTimeout(recurse, 3000);
        return;
      }
      onUpdate(id, result);
    }
    recurse();
  }

  useEffect(() => {
    fetch();
  }, []);

  if (request.error) {
    return <p>{JSON.stringify(request.error)}</p>;
  }

  if (request.status === api.STATUS.READY) {
    const { match } = request;

    if (match) {
      return (
        <p>
          ✅ Found sequence <strong>{id}</strong> in genome{" "}
          <strong>{request.match.genome_id}</strong>: protein{" "}
          <strong>{request.match.protein_id}</strong>, position{" "}
          <strong>{request.match.position}</strong>
        </p>
      );
    }

    return (
      <p>
        ❌ Couldn't find sequence <strong>{id}</strong>
      </p>
    );
  }

  return (
    <p>
      ⏳ Searching for <strong>{id}</strong>...
    </p>
  );
}

function App() {
  const [searchSequence, setSearchSequence] = useState("");
  const [requests, setRequests] = useLocalStorage({});

  const handleSubmit = useCallback(
    (event) => {
      event.preventDefault();

      if (searchSequence.length === 0) {
        return;
      }

      setRequests((requests) => ({
        ...requests,
        [searchSequence.toUpperCase()]: {
          startedOn: Date.now(),
          status: api.STATUS.LOADING,
        },
      }));
      setSearchSequence("");
    },
    [searchSequence]
  );

  const handleUpdate = useCallback(
    (id, newState) => {
      setRequests((requests) => ({
        ...requests,
        [id]: {
          ...requests[id],
          ...newState,
        },
      }));
    },
    [requests]
  );

  return (
    <div className="App">
      <h1>DNA search</h1>
      <form onSubmit={handleSubmit}>
        <input
          pattern="[AaTtCcGg]+"
          placeholder="Enter DNA sequence, e.g. GATTACA..."
          type="search"
          value={searchSequence}
          onChange={(event) => {
            setSearchSequence(event.target.value);
          }}
        />
      </form>

      <h2>Requests</h2>
      <div>
        {Object.keys(requests).length === 0 ? (
          <p>No previous requests.</p>
        ) : (
          <div>
            <button
              onClick={() => {
                setRequests({});
              }}
            >
              Clear
            </button>
            {Object.entries(requests)
              .sort((a, b) => b[1].startedOn - a[1].startedOn)
              .map(([id, request]) => (
                <Request
                  key={id}
                  id={id}
                  request={request}
                  onUpdate={handleUpdate}
                />
              ))}
          </div>
        )}
      </div>
    </div>
  );
}

export default App;

const TEST_REQUESTS = {
  GATTACA: {
    status: api.STATUS.READY,
    match: {
      genome_id: "GID",
      protein_id: "PID",
      position: 740,
    },
  },
  BOL: {
    status: api.STATUS.LOADING,
  },
  ZABA: {
    status: api.STATUS.READY,
    match: undefined,
  },
};
