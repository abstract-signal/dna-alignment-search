# Download FASTA files to ./data

import os
from Bio import Entrez, SeqIO
from config import genome_list_ids

Entrez.email = 'abstract-signal@proton.me'

def download_data(id):
    print(f"Fetching {id}")

    # genbank
    handle = Entrez.efetch(db="nuccore", id=id, rettype="gb", retmode="text")
    genome = SeqIO.read(handle, "gb")

    dirname = os.path.dirname(__file__)
    path = os.path.join(dirname, "data", f"{id}.gb")

    with open(path, "w") as output_handle:
        SeqIO.write(genome, output_handle, "gb")

if __name__ == '__main__':
    for genome_id in genome_list_ids:
        download_data(genome_id)
