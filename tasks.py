import os
import celery
import search

CELERY_BROKER_URL = os.environ['REDIS_URL']
CELERY_BACKEND_URL = os.environ['REDIS_URL']
celery_app = celery.Celery('main', broker=CELERY_BROKER_URL, backend=CELERY_BACKEND_URL)

@celery_app.task
def find_sequence(dna):
    return search.find_sequence(dna)