import glob

from Bio import SeqIO
from Bio.Seq import Seq 

def find_sequence(sequence):
    needle = Seq(sequence)
    for path in glob.glob("./data/*.gb"):
        for gb_record in SeqIO.parse(path, "genbank"):
            for feature in gb_record.features:
                if feature.type != "CDS":
                    continue
                protein_id = feature.qualifiers['protein_id'][0]
                sequence = feature.extract(gb_record.seq)
                match = sequence.find(needle)
                if match != -1:
                    return {
                        "genome_id": gb_record.id,
                        "protein_id": protein_id,
                        "position": match
                    }
    return None