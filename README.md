# App

https://dna-alignment-search.herokuapp.com

![Screenshot](./other/screenshot.png)

# Notes

I've spent around 5-6 hours on this project.

I've initially started with the very naive task queue implementation using threads and in-memory storage. But given the requirement that I need to deploy this somewhere, I realized it would not work with the most deployment platforms (like Heroku). So I've added Celery with a storage backed by Redis.

I've spent most time matching the task to the Biopython APIs. From what I understood, there is a more efficient way to do the required search called BLAST, however at that point I've decided not to spent more time on this part. My implementation is a naive loop.

A few other notes:

- In real world, it would be useful to have limits on the simultaneous task execution. Another useful thing would be to cache search results.
- Here I am using app server to host frontend files. In production I would choose a separate tool, like Nginx.
- There is a naive script to test the API: `./api_test`. I've decided not to spend time on tests, but obviously those are always a good idea when writing for production.
- I never used Django and had some experience with Flask, this is why I chose Flask instead of Django.
- Reading GenBank files (`./data/*.gb`) from disk during requests is not ideal, in real-world I would put them into some cache/database or maybe even in memory depending on the size.
- If I had more time, I would pack this into a Docker container. I saw the requirement about one command build/run, but I had some problems with Docker on my machine, so I decided not to spend more time on it.

# Install

```bash
# Install backend dependencies
pip install -r requirements.txt

# Install frontend dependencies (needs Node)
cd client && npm install

# Download GenBank data
python3 prepare.py
```

# Run (macOS)

```bash
# Start Redis if needed
redis-server

# Get Heroku CLI: https://devcenter.heroku.com/articles/heroku-cli
brew tap heroku/brew && brew install heroku

REDIS_URL='redis://localhost:6379' heroku local -p 7123
```